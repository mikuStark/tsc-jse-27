package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.EmptyDomainException;

public abstract class AbstractDataCommand extends AbstractCommand {


    @NonNull
    protected static final String FILE_BINARY = "./data.bin";
    @NonNull
    protected static final String FILE_BINARY64 = "./data.base64";

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @NonNull
    public Domain getDomain() {
        @NonNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    public @Nullable
    abstract Role[] roles();
}
