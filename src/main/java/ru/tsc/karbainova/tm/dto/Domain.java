package ru.tsc.karbainova.tm.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

@Getter
public class Domain implements Serializable {
    @NonNull
    private List<User> users;
    @NonNull
    private List<Project> projects;
    @NonNull
    private List<Task> tasks;

    public void setUsers(@NonNull List<User> users) {
        this.users = users;
    }

    public void setProjects(@NonNull List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@NonNull List<Task> tasks) {
        this.tasks = tasks;
    }

}
